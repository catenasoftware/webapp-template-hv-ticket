package software.catena.webapp.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table("authorities")
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Authority {

    @Id
    @Column("id")
    private Long id;

    @Column("username")
    @ToString.Include
    private String username;

    @Column("authority")
    @ToString.Include
    private String authority;

}
