package software.catena.webapp.backend.config;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import software.catena.webapp.backend.model.Authority;
import software.catena.webapp.backend.model.User;
import software.catena.webapp.backend.repository.AuthorityRepository;
import software.catena.webapp.backend.repository.UserRepository;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@Component
public class SetupDefaultData {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    /**
     * On startup of the application, the default user 'catena-manager' with
     * password 'catena-manager' is created if it does not exists
     */
    @EventListener(ApplicationReadyEvent.class)
    public void loadData() {
        String catenaUsername = "catena-manager";

        boolean defaultUserExists = userRepository.findByUsername(catenaUsername).count() > 0;

        if (!defaultUserExists) {

            User catenaManager = new User();
            catenaManager.setFirstname("catena");
            catenaManager.setLastname("manager");
            catenaManager.setUsername(catenaUsername);
            // Password is hashed using BCryptPasswordEncoder
            catenaManager.setPassword(new BCryptPasswordEncoder().encode("catena-manager"));
            catenaManager.setEnabled(true);
            userRepository.save(catenaManager);

            Authority catenaAuthority = new Authority();
            catenaAuthority.setUsername(catenaUsername);
            catenaAuthority.setAuthority("ROLE_USER");
            authorityRepository.save(catenaAuthority);
        }
    }
}
