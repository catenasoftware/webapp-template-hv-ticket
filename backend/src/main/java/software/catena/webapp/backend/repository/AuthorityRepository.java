package software.catena.webapp.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import software.catena.webapp.backend.model.Authority;

import java.util.stream.Stream;

@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    Stream<Authority> findByUsername(String username);
}
