package software.catena.webapp.backend.exceptions;

public class UserAlreadyExists extends RuntimeException {

    public UserAlreadyExists(String username) {
        super("User with username '" + username + "' already exists.");
    }

}
