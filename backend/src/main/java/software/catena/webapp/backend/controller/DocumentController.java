package software.catena.webapp.backend.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import software.catena.webapp.backend.exceptions.UserAlreadyExists;
import software.catena.webapp.backend.exceptions.UserNotFoundException;
import software.catena.webapp.backend.model.Authority;
import software.catena.webapp.backend.model.User;
import software.catena.webapp.backend.repository.AuthorityRepository;
import software.catena.webapp.backend.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class DocumentController {

    private final String BASE_PATH = "/api/documents";
    // add here your documents endpoints (see UserController)
}
