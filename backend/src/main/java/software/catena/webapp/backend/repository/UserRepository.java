package software.catena.webapp.backend.repository;

import java.util.stream.Stream;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import software.catena.webapp.backend.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Stream<User> findByFirstname(String firstname);
    Stream<User> findByUsername(String username);
}
