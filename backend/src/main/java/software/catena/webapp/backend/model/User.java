package software.catena.webapp.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table("users")
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class User {

    @Id
    @Column("id")
    private Long id;

    @Column("username")
    @ToString.Include
    private String username;

    @Column("firstname")
    @ToString.Include
    private String firstname;

    @Column("lastname")
    @ToString.Include
    private String lastname;

    @Column("password")
    @ToString.Include
    private String password;

    @Column("enabled")
    @ToString.Include
    private boolean enabled;
}
