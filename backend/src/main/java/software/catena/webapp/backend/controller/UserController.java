package software.catena.webapp.backend.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import software.catena.webapp.backend.exceptions.UserAlreadyExists;
import software.catena.webapp.backend.exceptions.UserNotFoundException;
import software.catena.webapp.backend.model.Authority;
import software.catena.webapp.backend.model.User;
import software.catena.webapp.backend.repository.AuthorityRepository;
import software.catena.webapp.backend.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final String BASE_PATH = "/api/users";

//    SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    @GetMapping(value = BASE_PATH + "/auth/{username}")
    public User getUserById(@PathVariable String username) {
        return userRepository.findByUsername(username).
                findFirst().
                orElseThrow(() -> new UserNotFoundException(username));
    }

    @GetMapping(value = BASE_PATH)
    public List<User> all() {
        return (List<User>) userRepository.findAll();
    }

    @GetMapping(value = BASE_PATH + "/{id}")
    public User getUserById(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @PostMapping(value = BASE_PATH)
    public User postUser(@RequestBody User user) {
        if (userRepository.findByUsername(user.getUsername()).count() != 0){
            throw new UserAlreadyExists(user.getUsername());
        }
        // 1. create a new user
        user.setId(null);
        user.setEnabled(true);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        User newUser = userRepository.save(user);
        // 2. create a new authority for this user to match the Spring basic auth schema
        Authority userAuthority = new Authority();
        userAuthority.setUsername(user.getUsername());
        userAuthority.setAuthority("ROLE_USER");
        authorityRepository.save(userAuthority);

        return newUser;
    }
// PUT Method not supported yet
//    @PutMapping(value = BASE_PATH + "/{id}")
//    public User replaceEmployee(@RequestBody User newEmployee, @PathVariable Long id) {
//
//        return userRepository.findById(id).map(user -> {
//            user.setFirstname(newEmployee.getFirstname());
//            return userRepository.save(user);
//        }).orElseGet(() -> {
//            newEmployee.setId(null);
//            return userRepository.save(newEmployee);
//        });
//    }

    @DeleteMapping(value = BASE_PATH + "/{id}")
    public void deleteUser(@PathVariable Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isEmpty()) {
            Stream<Authority> deleteAuthority = authorityRepository.findByUsername(user.get().getUsername());
            deleteAuthority.forEach( a -> authorityRepository.deleteById(a.getId()));
            userRepository.deleteById(id);
        }
    }

}
