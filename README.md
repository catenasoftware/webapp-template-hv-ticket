# Webapp Template

This project is a template for C_Chain webapps and contains:

- Angular Project (https://angular.io/docs) called app
- Spring Boot (https://spring.io) Project called backend which is a stateless server that provides a REST API
- PostgreSQL Database which can be used by the backend
- C_Chain Foundation, the library to communicate with the C_Chain Manager (TODO)
- Google Chrome Browser. Use the dev tools (https://developers.google.com/web/tools/chrome-devtools)

## Setup

### Required Tools

- IntelliJ IDEA Ultima Version (The community version is also ok). Get a student licence from here: https://www.jetbrains.com/de-de/idea/ 
- Docker for your desktop https://www.docker.com/get-started
- Node at least version 12 (https://nodejs.org/en/)
    - Homebrew for MacOs possible: https://formulae.brew.sh/formula/node
    - For Linux install it from your package manager
- yarn as package manager for the angular app
    - choose your OS: https://classic.yarnpkg.com/en/docs/install/#mac-stable
- Postman (recommend for testing the REST API)
    - See https://www.postman.com

### IDE Setup
After installing all the required tools, setup your IDE:

- Open IntelliJ and go to File -> Open and open the folder Software_Catena_Webapp
- Go to backend/build.gradle and import as gradle project (right click)

## Start the Application
- Go to backend/build.gradle and execute devStart. This will start the PostreSQL in a docker container. Without this step the backend will not start.
- Go to backend/src/main/java/software/catena/webapp/backend/BackendApplication.java
- Click the green arrow and run the application
- Go to app/package.json and execute yarn install and then start

## Spotless

Spotless is used for formatting the spring application.
Go to gradle -> Tasks -> other -> spotlessJava to apply the spotless rules

## Liquibase

- The schema of the postgres is defined in liquibase/0/release-changelog.xml
- The schema is applied on startup
- Do not edit already applied changelogs
- The applied changelog can be found in the table databasechangelog (select * from databasechangelog)
- Liquibase data types: https://dba-presents.com/index.php/liquibase/216-liquibase-3-6-x-data-types-mapping-table
- How Liquibase works: https://www.liquibase.org/get-started/how-liquibase-works

## Basic Auth

- For simple start, basic auth is used to authenticate the users
- By default the user "catena-manager" with password "catena-manager" is created. Use this user to logon the webapp for the first time
- All users that are created under 'users' in the webapp can be used for login
- So, a simple multiuser application can be used to demonstrate interaction between different parties
- More: https://www.baeldung.com/spring-security-basic-authentication

## Angular Development

- Also see https://angular.io/cli/generate to see how to use ng generate 

### Create a new component

- create a new component in a dedicated folder
- ng generate component <COMPONENET_NAME>

### Create a new service

- ng generate service <SERVICE_NAME>

### PrimeNG

This project is using PrimeNg (https://www.primefaces.org/primeng/showcase/#/) as UI Library.
This Library provides:

- more than 70 components (e.g Tables, Buttons, etc) in a modern and clean design
- code snippets for each component (nice for fast development)

### Mobile Support

PrimeNg includes Primeflex (https://www.primefaces.org/primeng/showcase/#/primeflex) which is a flexible grid system that also supports 
responsive behavior.
All pages should be responsive to support mobile support. Use Chrome to test also the view of mobile devices (https://developers.google.com/web/tools/chrome-devtools)

### Authentication

This App is using the auth-guard (app/src/app/authentication/auth-guard.guard.ts) to protect pages against unauthorized access.
For development, it is useful to change the lines in authetication.service.ts (app/src/app/authentication/authentication.service.ts) to this:

  - private username = 'catena-manager'; 
  - private password = 'catena-manager';
  - private isAuth = true;

  - // private username: string;
  - // private password: string;
  - // private isAuth = false;
  
So you do not have to login everytime you make a change in your angular application.

## C_Ground

From the login page you can go to the so called C_Ground. There, you can discover the functionality 
of C_Chain like creating C_Accounts, C_Chains and C_Blocks.
