import { Component } from '@angular/core';
import {RoutingService} from './routing/routing.service';
import {AuthenticationService} from './authentication/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authenticationService: AuthenticationService, private routingService: RoutingService) {
  }

  title = 'app';
  items = [{
    label: 'Prozess',
    items: [
      {
      label: 'Prozess Assistent',
      icon: 'pi pi-forward',
      // command: () => {
      //   this.update();
      // }
    },
      {
        label: 'Mieter Anmelden',
        icon: 'pi pi-user',
        command: () => {
          this.nagivateRegisterTenat();
        }
      },
    ]},
    {
      label: 'Options',
      items: [{
        label: 'Logout',
        icon: 'pi pi-sign-out',
        command: () => {
          this.authenticationService.signOut();
        }
      },
      ]}
  ];

  isAuthenticated(): boolean {
    return this.authenticationService.isAuthenticated();
  }

  nagivateRegisterTenat(): void {
    this.routingService.navigateRegisterTenat();
  }
}
