import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './routing/app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { UserTableComponent } from './users/user-table.component';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import { MenubarModule } from 'primeng/menubar';
import { CardModule } from 'primeng/card';
import { UserFormComponent } from './users/user-form/user-form.component';
import { ToolbarModule } from 'primeng/toolbar';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import { TabViewModule } from 'primeng/tabview';
import { LoginComponent } from './login/login.component';
import {PasswordModule} from 'primeng/password';
import {DialogModule} from 'primeng/dialog';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ToastModule} from 'primeng/toast';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DropdownModule} from 'primeng/dropdown';
import {EditorModule} from 'primeng/editor';
import {SidebarModule} from 'primeng/sidebar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MultiSelectModule} from 'primeng/multiselect';
import {ChartModule} from 'primeng/chart';
import {MenuModule} from 'primeng/menu';
import {StepsModule} from 'primeng/steps';

import {httpInterceptorProviders} from './http-interceptors';
import { SettingsComponent } from './settings/settings.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { DocumentsComponent } from './documents/documents.component';
import {RippleModule} from 'primeng/ripple';
import {AccountsBoardComponent} from './cground/accounts-board/accounts.board.component';
import {AccountComponent} from './cground/account/account.component';
import { BlocksComponent } from './cground/blocks/blocks.component';
import { PermissionsComponent } from './cground/permissions/permissions.component';
import { TicketsComponent } from './tickets/tickets/tickets.component';
import { TicketViewComponent } from './tickets/ticket-view/ticket-view.component';
import { RealEstateViewComponent } from './documents/real-estate-view/real-estate-view.component';
import { RegisterTenatProcessComponent } from './register-tenat-process/register-tenat-process.component';
import { TenatDataComponent } from './register-tenat-process/tenat-data/tenat-data.component';
import { BankAccountStepComponent } from './register-tenat-process/bank-account-step/bank-account-step.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserTableComponent,
    UserFormComponent,
    LoginComponent,
    SettingsComponent,
    FeedbackComponent,
    DocumentsComponent,
    AccountsBoardComponent,
    AccountComponent,
    BlocksComponent,
    PermissionsComponent,
    TicketsComponent,
    TicketViewComponent,
    RealEstateViewComponent,
    RegisterTenatProcessComponent,
    TenatDataComponent,
    BankAccountStepComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    TableModule,
    FormsModule,
    ButtonModule,
    ConfirmDialogModule,
    MenubarModule,
    CardModule,
    ToolbarModule,
    InputMaskModule,
    InputTextModule,
    TabViewModule,
    PasswordModule,
    DialogModule,
    BrowserAnimationsModule,
    InputTextareaModule,
    ToastModule,
    RippleModule,
    InputSwitchModule,
    DropdownModule,
    EditorModule,
    SidebarModule,
    MultiSelectModule,
    ChartModule,
    MenuModule,
    StepsModule,
  ],
  providers: [HttpClientModule, ConfirmationService, httpInterceptorProviders, ToastModule, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
