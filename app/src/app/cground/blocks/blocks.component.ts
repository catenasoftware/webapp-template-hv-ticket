import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {ActivatedRoute} from '@angular/router';
import {PlaygroundService, Account} from '../playground.service';

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.scss']
})
export class BlocksComponent implements OnInit {

  blocks: any[] = [];
  accountId: number;
  chainId: string;
  selectedBlockId: number;
  displayCreateBlocksDialog = false;
  displayPayloadDialog = false;
  cAccounts: Account[] = [];
  receiver: Account;
  selectedPayload = '';

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private playgroundService: PlaygroundService) { }

  async ngOnInit(): Promise<void> {
    this.accountId = parseInt(this.route.snapshot.paramMap.get('accountId'), 10);
    this.chainId = this.route.snapshot.paramMap.get('chainId');
    await this.loadBlocks();
    this.cAccounts = this.playgroundService.getAllAccounts();
    this.receiver = this.cAccounts[0];
  }

  async loadBlocks(): Promise<void> {
    this.blocks = await this.playgroundService.getAccountById(this.accountId).ccf.getAllBlocksList(this.chainId);
    console.log(this.blocks.length);
  }

  decodeBase64(payload: string): string {
    return atob(payload);
  }

  getNameFromPublicKey(publicKey: string): string {
    return this.playgroundService.getAccountNameByPublicKey(publicKey);
  }

  getAccountName(): string {
    return this.playgroundService.getAccountById(this.accountId).name;
  }

  getBlockTypeDescription(type: number): string {
    if (type === 0) {
      return 'Chain Creation C_Block';
    } else if (type === 1) {
      return 'Chain Update C_Block';
    } else if (type === 2 || type === 3) {
      return 'Permission C_Block';
    } else {
      return 'Application C_Block';
    }
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleString();
  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  toggleCreateBlockDialog(): void {
    this.displayCreateBlocksDialog = !this.displayCreateBlocksDialog;
  }

  togglePayloadDialog(selectedPayload: string): void {
    try {
      this.selectedPayload = JSON.stringify(JSON.parse(selectedPayload), null, 2);
    }catch (e) {
      this.selectedPayload = selectedPayload;
    }
    this.displayPayloadDialog = !this.displayPayloadDialog;
  }

  async createBlock(payload: string, type: number): Promise<void> {
    if (payload && type && this.receiver) {
      const ccf = await this.playgroundService.getAccountById(this.accountId).ccf;
      await ccf.createBlock(this.chainId, this.receiver.ccf.getCryptId(), type, payload);
      await this.loadBlocks();
      this.toggleCreateBlockDialog();
    }
  }
}
