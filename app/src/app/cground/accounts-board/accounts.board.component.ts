import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {PlaygroundService, Account} from '../playground.service';

@Component({
  selector: 'app-cground',
  templateUrl: './accounts.board.component.html',
  styleUrls: ['./accounts.board.component.scss']
})
export class AccountsBoardComponent implements OnInit {

  accounts: Account[] = [];
  displayCreateAccountDialog = false;
  newAccountName = '';

  constructor(private routingService: RoutingService,
              private playgroundService: PlaygroundService) { }

  ngOnInit(): void {
    this.loadAccounts();
    console.log(this.accounts);
  }

  async loadAccounts(): Promise<void> {
    this.accounts = this.playgroundService.getAllAccounts();
  }

  navigateAccount(id: number): void {
    this.routingService.navigateAccount(id.toString());
  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  showCreateAccountDialog(): void {
    this.displayCreateAccountDialog = true;
  }

  hideCreateAccountDialog(): void {
    this.displayCreateAccountDialog = false;
  }

  createAccount(): void {
    if (this.newAccountName) {
      this.playgroundService.addAccount(this.newAccountName);
      this.loadAccounts();
      this.hideCreateAccountDialog();
      this.newAccountName = '';
    }
  }

}
