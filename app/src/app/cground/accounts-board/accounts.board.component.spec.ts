import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsBoardComponent } from './accounts.board.component';

describe('CgroundComponent', () => {
  let component: AccountsBoardComponent;
  let fixture: ComponentFixture<AccountsBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountsBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
