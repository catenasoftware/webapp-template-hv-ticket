import {Injectable, Optional} from '@angular/core';
import CCF from '../ccf-web/ccf-web';

export interface Account {
  id: number;
  name: string;
  ccf: CCF;
}


@Injectable({
  providedIn: 'root'
})
export class PlaygroundService {

  filterUser: Account;

  constructor() {
    this.accounts[0].ccf.createChain();
  }

  private accounts: Account[] = [
    {id: 0, name: 'Impfzentrum München', ccf: new CCF('localhost', 4200)},
    {id: 1, name: 'Lufthansa', ccf: new CCF('localhost', 4200)}
  ];
  private nextId = 2;

  getAllAccounts(): Account[] {
    return this.accounts;
  }

  addAccount(name: string): void {
    this.accounts.push({id: this.nextId, name, ccf: new CCF('localhost', 4200)});
    this.nextId = this.nextId + 1;
  }

  getAccountById(id: number): Account {
    return this.accounts[id];
  }

  getAccountNameByPublicKey(publicKey: string): string {
    const account = this.accounts.find(a => a.ccf.getCryptId().publicKey === publicKey);
    if (!account) {
      return 'CCM';
    }
    return account.name;
  }

  getAccountNameById(id: number): string {
    console.log(id);
    return this.accounts[id].name;
  }

  getAccounts(): Account[] {
    return this.accounts;
  }

  getChainOwnerFilter(): Account {
    return this.filterUser;
  }

  setChainOwnerFilter(param?: Account): void {
    this.filterUser = param;
  }
}
