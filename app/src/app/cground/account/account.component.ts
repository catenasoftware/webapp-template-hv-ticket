import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {PlaygroundService, Account} from '../playground.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-cground',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  chains: any[] = [];
  accountId: number;
  accountName: string;

  selectedUpdateChain: any = {};



  allAccounts: Account[] = [];

  checked: true;
  displayCreateChainDialog = false;
  displayUpdateChainDialog = false;

  contextUser: Account;
  filterUser: Account;
  userShowChains: Account;

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private playgroundService: PlaygroundService) { }


  ngOnInit(): void {
    this.accountId = parseInt(this.route.snapshot.paramMap.get('accountId'), 10);
    this.contextUser =  this.playgroundService.getAccountById(this.accountId);

    this.accountName = this.getAccountName();
    this.allAccounts = this.playgroundService.getAllAccounts();
    const loadedFilterUser = this.playgroundService.getChainOwnerFilter();
    this.userShowChains =  loadedFilterUser ? loadedFilterUser : this.contextUser;
    this.filterUser = this.userShowChains;

    this.loadChains();
  }

  async createChain(name: string, description: string, useDefaultNameAndDescription: boolean): Promise<void> {
    const chain = await this.playgroundService.getAccountById(this.accountId).ccf.createChain();
    if (!useDefaultNameAndDescription) {
      await this.playgroundService.getAccountById(this.accountId).ccf.updateChain(chain.data[0].ID, name, description, false, false, false);
    }
    await this.loadChains();
    this.triggerShowCreateChain();
  }

  async reloadChains(): Promise<void> {
    this.userShowChains = this.filterUser;
    this.playgroundService.setChainOwnerFilter(this.userShowChains);
    await this.loadChains();
  }

  async loadChains(): Promise<void> {
    const ccfContext = this.contextUser.ccf;
    const ccfShowUserChains = this.userShowChains.ccf;
    const chains = (await ccfContext.getChains({ownerPublicKey: ccfShowUserChains.getCryptId().publicKey})).data;
    this.chains = chains;
  }

  async updateChain(chainID: string,
                    name: string,
                    description: string,
                    closed: boolean,
                    publicRead: boolean,
                    publicWrite: boolean): Promise<void> {
    await this.playgroundService.getAccountById(this.accountId).ccf.updateChain(
      chainID,
      name,
      description,
      closed,
      publicRead,
      publicWrite);
    await this.loadChains();
    this.triggerShowUpdateChain();
  }

  getAccountName(): string {
    return this.playgroundService.getAccountNameById(this.accountId);
  }


  getNameFromPublicKey(publicKey: string): string {
    return this.playgroundService.getAccountNameByPublicKey(publicKey);
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleString();
  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  navigateBlocks(chainId: string): void {
    this.routingService.navigateBlocks(this.accountId.toString(), chainId);
  }

  navigatePermissions(chainId: string): void {
    this.routingService.navigatePermissions(this.accountId.toString(), chainId);
  }

  triggerShowCreateChain(): void {
    this.displayCreateChainDialog = !this.displayCreateChainDialog;
  }

  triggerShowUpdateChain(selectedChain?: any): void {
    if (selectedChain) {
      this.selectedUpdateChain = JSON.parse(JSON.stringify(selectedChain));
    }
    this.displayUpdateChainDialog = !this.displayUpdateChainDialog;
  }
}
