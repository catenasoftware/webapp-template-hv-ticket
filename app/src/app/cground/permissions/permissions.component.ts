import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {PlaygroundService, Account} from '../playground.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

  showCreatPermissionDialog = false;

  acls: any[] = [];
  accountId: number;
  chainId: string;

  accountName: string;
  cAccounts: Account[] = [];

  constructor(private routingService: RoutingService,
              private playgroundService: PlaygroundService,
              private route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.accountId = parseInt(this.route.snapshot.paramMap.get('accountId'), 10);
    this.chainId = this.route.snapshot.paramMap.get('chainId');
    this.accountName = this.getAccountName();
    this.cAccounts = this.playgroundService.getAllAccounts().filter(e => e.id !== this.accountId);
    await this.loadAcls();
  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  triggerPermissionDialog(): void {
    this.showCreatPermissionDialog = !this.showCreatPermissionDialog;
  }

  async loadAcls(): Promise<void> {
    this.acls = [];
    const ccf = this.playgroundService.getAccountById(this.accountId).ccf;
    this.acls = await ccf.getAclList(this.chainId, {showAll: true});
  }

  async creatAclPopup(readPermission: boolean, writePermission: boolean, doDelete: boolean, receiverAccount: Account): Promise<void> {
    await this.createAcl(readPermission, writePermission, doDelete, receiverAccount.ccf.getCryptId().publicKey);
  }

  async createAcl(readPermission: boolean, writePermission: boolean, doDelete: boolean, receiverPublicKey: string): Promise<void> {
    // if (!this.selectedReceiver && !publicKey) {
    //   await this.presentAlert('Bitte wähle einen C_Account aus.');
    //   return;
    // }
    const myCcf = this.playgroundService.getAccountById(this.accountId).ccf;
    await myCcf.updateAcl(this.chainId,
      [
        {
          publicKey: receiverPublicKey,
          read: readPermission,
          write: writePermission,
          delete: doDelete
        }
      ]);
    // await this.presentToast('Permission wurde gebucht.');
    await this.loadAcls();
    this.showCreatPermissionDialog = false;
  }


  getAccountName(): string {
    return this.playgroundService.getAccountNameById(this.accountId);
  }


  getNameFromPublicKey(publicKey: string): string {
    return this.playgroundService.getAccountNameByPublicKey(publicKey);
  }

  formatDate(date: string): string {
    return new Date(date).toLocaleString();
  }

}
