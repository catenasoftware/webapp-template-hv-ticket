import { Injectable } from '@angular/core';
import {MessageService} from 'primeng/api';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppNotificationService {

  constructor(private messageService: MessageService) { }

  private life = 7000;
  private SUCCESS = 'success';
  private INFO = 'info';
  private WARN = 'warn';
  private ERROR = 'error';

  private standardSummary = 'Service Message';


  addSuccess(errorDetail: string, messageSummary: string = this.standardSummary): void {
    this.addMessage(this.SUCCESS, messageSummary, errorDetail);
  }

  addInfo( errorDetail: string, messageSummary: string = this.standardSummary): void {
    this.addMessage(this.INFO, messageSummary, errorDetail);
  }

  addWarn(errorDetail: string, messageSummary: string = this.standardSummary): void {
    this.addMessage(this.WARN, messageSummary, errorDetail);
  }

  addError(errorDetail: string, messageSummary: string = this.standardSummary): void {
    this.addMessage(this.ERROR, messageSummary, errorDetail);
  }

  addErrorError(error: HttpErrorResponse, messageSummary: string = this.standardSummary): void {
    this.addMessage(this.ERROR, messageSummary, error.error);
  }


  private addMessage(messageSeverity: string, messageSummary: string, messageDetail: string): void {
    this.messageService.add({
      severity: messageSeverity,
      summary: messageSummary,
      detail: messageDetail,
      life: this.life});
  }

}
