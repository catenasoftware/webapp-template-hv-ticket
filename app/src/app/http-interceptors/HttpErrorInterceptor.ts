import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpErrorResponse
} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AppNotificationService} from '../notification/app-notification.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private appNotificationService: AppNotificationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /**
     *  For all http responses, the error message is displayed here using the custom AppNotificationService
     */
    return next.handle(req).pipe(catchError((error: HttpErrorResponse) => {
      if (error.status < 300){
        // No error
      } else if (error.status < 500) {
        if (error.status === 401) {
          // Do not display authentication error via notification service
          return throwError(error);
        }
        // Client Error
        this.appNotificationService.addErrorError(error);
      } else if (error.status < 512){
        // Server Error
        this.appNotificationService.addError('Server did not respond properly.');
      }
      return throwError(error);
    }));
  }
}
