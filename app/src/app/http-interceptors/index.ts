import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthenticationInterceptor} from './AuthenticationInterceptor';
import {HttpErrorInterceptor} from './HttpErrorInterceptor';


/** Http interceptor providers in outside-in order. See https://angular.io/guide/http#intercepting-requests-and-responses */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }
];
