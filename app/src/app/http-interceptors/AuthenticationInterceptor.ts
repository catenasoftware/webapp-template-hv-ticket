import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders
} from '@angular/common/http';

import { Observable } from 'rxjs';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /**
     *  For all http requests, add authentication at this central point.
     */
    const authReq = req.clone({
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-Requested-With': 'XMLHttpRequest', // append this to avoid basic auth window pop up
        Authorization: 'Basic ' + btoa(this.auth.getUsername() + ':' + this.auth.getPassword())
      })
    });
    return next.handle(authReq);
  }
}
