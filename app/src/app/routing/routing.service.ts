import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {
  ACCOUNT,
  ALL_OTHER, BANK_STEP,
  BLOCKS,
  CGROUND, DOCUMENTS,
  FEEDBACK,
  HOME,
  LOGIN, PERMISSIONS, REAL_ESTATE_VIEW, REGISTER_TENANT,
  SETTINGS, TENANT_STEP, TICKET, TICKETS,
  USER_FORM,
  USERS_TABLE
} from './routes';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor(private router: Router, private route: ActivatedRoute, private location: Location) {}

  navigateLogin(): void{
    this.router.navigate([LOGIN], { relativeTo: this.route });
  }

  navigateHome(): void {
    this.router.navigate([HOME], { relativeTo: this.route });
  }

  navigateUsersTable(): void {
    this.router.navigate([USERS_TABLE], { relativeTo: this.route });
  }

  navigateUserForm(): void {
    this.router.navigate([USER_FORM], { relativeTo: this.route });
  }

  navigateSettings(): void {
    this.router.navigate([SETTINGS], { relativeTo: this.route });
  }

  navigateFeedback(): void {
    this.router.navigate([FEEDBACK], { relativeTo: this.route });
  }

  navigateDocuments(): void {
    this.router.navigate([DOCUMENTS], { relativeTo: this.route });
  }

  navigateAccountBoard(): void {
    this.router.navigate([CGROUND], { relativeTo: this.route });
  }

  navigateAccount(accountId: string): void {
    this.router.navigate([ACCOUNT, accountId], { relativeTo: this.route });
  }

  navigateBlocks(accountId: string, chainId: string): void {
    this.router.navigate([BLOCKS, accountId, chainId], { relativeTo: this.route });
  }

  navigatePermissions(accountId: string, chainId: string): void {
    this.router.navigate([PERMISSIONS, accountId, chainId], { relativeTo: this.route });
  }

  navigateBack(): void {
    this.location.back();
  }

  navigateTickets(): void {
    this.router.navigate([TICKETS], { relativeTo: this.route });
  }

  navigateTicket(ticketNumber: string): void {
    this.router.navigate([TICKET, ticketNumber], { relativeTo: this.route });
  }

  navigateRealEstate(id: string): void {
    this.router.navigate([REAL_ESTATE_VIEW, id], { relativeTo: this.route });
  }

  navigateRegisterTenat(): void {
    this.router.navigate([REGISTER_TENANT], { relativeTo: this.route });
  }

  navigateTenantStep(): void {
    this.router.navigate([ REGISTER_TENANT + '/' + TENANT_STEP], { relativeTo: this.route });
  }

  navigateBankAccountStep(): void {
    this.router.navigate([ REGISTER_TENANT + '/' + BANK_STEP], { relativeTo: this.route });
  }
}
