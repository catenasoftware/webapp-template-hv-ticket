import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserTableComponent } from '../users/user-table.component';
import {HomeComponent} from '../home/home.component';
import {UserFormComponent} from '../users/user-form/user-form.component';
import {LoginComponent} from '../login/login.component';
import {AuthGuardGuard} from '../authentication/auth-guard.guard';
import {SettingsComponent} from '../settings/settings.component';
import {FeedbackComponent} from '../feedback/feedback.component';
import {DocumentsComponent} from '../documents/documents.component';
import {AccountsBoardComponent} from '../cground/accounts-board/accounts.board.component';
import {AccountComponent} from '../cground/account/account.component';
import {BlocksComponent} from '../cground/blocks/blocks.component';
import {PermissionsComponent} from '../cground/permissions/permissions.component';
import {
  ACCOUNT, ACCOUNT_FULL,
  ALL_OTHER, BANK_STEP,
  BLOCKS, BLOCKS_FULL,
  CGROUND, DOCUMENTS,
  FEEDBACK,
  HOME,
  LOGIN, PERMISSIONS, PERMISSIONS_FULL, REAL_ESTATE_VIEW_FULL, REGISTER_TENANT,
  ROOT,
  SETTINGS, TENANT_STEP, TICKET, TICKET_FULL, TICKETS,
  USER_FORM,
  USERS_TABLE
} from './routes';
import {TicketsComponent} from '../tickets/tickets/tickets.component';
import {TicketViewComponent} from '../tickets/ticket-view/ticket-view.component';
import {RealEstateViewComponent} from '../documents/real-estate-view/real-estate-view.component';
import {RegisterTenatProcessComponent} from '../register-tenat-process/register-tenat-process.component';
import {TenatDataComponent} from '../register-tenat-process/tenat-data/tenat-data.component';
import {BankAccountStepComponent} from "../register-tenat-process/bank-account-step/bank-account-step.component";

/**
 *  All routes with canActivate: [AuthGuardGuard] can only be activated when login was successful
 */

const routes: Routes = [
  {path: USERS_TABLE, component: UserTableComponent, canActivate: [AuthGuardGuard]},
  {path: HOME, component: HomeComponent, canActivate: [AuthGuardGuard]},
  {path: CGROUND, component: AccountsBoardComponent},
  {path: ACCOUNT_FULL, component: AccountComponent},
  {path: BLOCKS_FULL, component: BlocksComponent},
  {path: PERMISSIONS_FULL, component: PermissionsComponent},
  {path: LOGIN, component: LoginComponent},
  {path: USER_FORM, component: UserFormComponent, canActivate: [AuthGuardGuard]},
  {path: SETTINGS, component: SettingsComponent, canActivate: [AuthGuardGuard]},
  {path: FEEDBACK, component: FeedbackComponent, canActivate: [AuthGuardGuard]},
  {path: DOCUMENTS, component: DocumentsComponent, canActivate: [AuthGuardGuard]},
  {path: TICKETS, component: TicketsComponent, canActivate: [AuthGuardGuard]},
  {path: TICKET_FULL, component: TicketViewComponent, canActivate: [AuthGuardGuard]},
  {path: REAL_ESTATE_VIEW_FULL, component: RealEstateViewComponent, canActivate: [AuthGuardGuard]},
  {path: REGISTER_TENANT, component: RegisterTenatProcessComponent, canActivate: [AuthGuardGuard],
    children: [
      {path: TENANT_STEP, component: TenatDataComponent},
      {path: BANK_STEP, component: BankAccountStepComponent},
    ]
  },
  {path: ROOT,   redirectTo: LOGIN, pathMatch: 'full'},
  {path: ALL_OTHER,   redirectTo: HOME, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
