import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../routing/routing.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  feedback: string;
  message: string;

  constructor(private routingService: RoutingService) { }

  ngOnInit(): void {
  }

  navigateHome(): void {
    this.routingService.navigateHome();
  }

  send(): void {
    if (this.feedback) {
      this.feedback = '';
      this.message = 'Thank you for your feedback!';
    }else{
      this.message = 'Please enter some feedback in the field above.';
    }
  }
}
