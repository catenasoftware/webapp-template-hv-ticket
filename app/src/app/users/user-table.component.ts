import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {User} from './model/user';
import {UserService} from './user.service';
import {RoutingService} from '../routing/routing.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  constructor(private httpClient: HttpClient, private userService: UserService, private routingService: RoutingService) {
  }

  cols: any[];
  users: User[] = [];
  selectedUser: User;


  ngOnInit(): void {
    this.cols = [
      {field: 'id', header: 'Id'},
      {field: 'username', header: 'Username'},
      {field: 'firstname', header: 'Firstname'},
      {field: 'lastname', header: 'Lastname'},
      {field: 'password', header: 'Password'}
    ];
    this.loadUsers();
  }

  loadUsers(): void {
    this.userService.getUsers().subscribe(data => this.users = data);
  }

  deleteUser(): void {
    this.userService.deleteUser(this.selectedUser.id).subscribe(() => {
      this.users.splice(this.users.indexOf(this.selectedUser), 1);
      this.selectedUser = null;
    });
  }

  navigateHome(): void {
    this.routingService.navigateHome();
  }

  navigateUserForm(): void {
    this.routingService.navigateUserForm();
  }
}


