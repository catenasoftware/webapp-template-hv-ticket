import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {RoutingService} from '../../routing/routing.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  @Input()
  id: number;
  firstname: string;
  lastname: string;
  password: string;
  username: string;

  constructor(private userService: UserService, private routingService: RoutingService) { }

  ngOnInit(): void {}

  /**
   *  Create a new user
   */
  createUser(): void{
    this.userService.postUser(
      {
        id: null,
        username: this.username,
        firstname: this.firstname,
        lastname: this.lastname,
        password: this.password
      }).subscribe(_ => {
        this.routingService.navigateUsersTable();
      }
    );
  }

  navigateUsersTable(): void {
    this.routingService.navigateUsersTable();
  }
}
