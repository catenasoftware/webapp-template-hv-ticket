import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './model/user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  postUser(user: User): Observable<User> {
    return this.httpClient.post<User>('/api/users', user);
  }

  deleteUser(id: number): Observable<User> {
    return this.httpClient.delete<User>('/api/users/' + id);
  }

  getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>('/api/users');
  }

}
