import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../routing/routing.service';
import {DocumentsService, RealEstate} from './documents.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  displaySearchBox = true;

  constructor(private routingService: RoutingService, private documentService: DocumentsService) { }

  realEstate: RealEstate[];

  searchParams = {
    name: '',
    city: '',
    address: '',
    zipCode: ''
  };

  ngOnInit(): void {
    this.realEstate = this.documentService.searchRealEstate();
  }

  navigateHome(): void {
    this.routingService.navigateHome();
  }

  triggerSearchBox(): void {
    this.displaySearchBox = !this.displaySearchBox;
  }

  navigateRealEstate(id: string): void {
    this.routingService.navigateRealEstate(id);
  }
}
