import { Injectable } from '@angular/core';
import {Category, Comment, LivingUnit, Priority, Status, User} from "../tickets/tickets.service";


export interface RealEstate {
  id: string;
  name: string;
  address: string;
  city: string;
  zipCode: number;
  numberOfFlats: number;
  numberOfRentFlats: number;
  tentants: User[];
  numberOfTentants: number;
}

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  // add here your server calls api/documents. see users.service
  constructor() { }

  realEstate = [
    {
      id: '0',
      name: 'Little Blue',
      address: 'Gärtnerplatz 1',
      city: 'München',
      zipCode: 80469,
      numberOfFlats: 37,
      numberOfRentFlats: 34,
      tentants: [],
      numberOfTentants: 83
    }
  ];

  searchRealEstate(): RealEstate[] {
    return this.realEstate;
  }

  loadRealEstate(id: string): RealEstate {
    return this.realEstate.find(r => r.id === id);
  }
}
