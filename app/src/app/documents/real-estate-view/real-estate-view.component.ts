import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {DocumentsService, RealEstate} from '../documents.service';
import {ActivatedRoute} from '@angular/router';
import {Ticket, TicketsService} from '../../tickets/tickets.service';

@Component({
  selector: 'app-real-estate-view',
  templateUrl: './real-estate-view.component.html',
  styleUrls: ['./real-estate-view.component.scss']
})
export class RealEstateViewComponent implements OnInit {
  data: any;
  payments: any;
  realEstate: RealEstate;
  tickets: Ticket[];
  users = [
    {
      name: 'Max Musermann',
      flat: 'Wohnung 13, 5. Stock',
      movedIn: '01.05.2021'
    },
    {
      name: 'Petra Musermann',
      flat: 'Wohnung 13, 5. Stock',
      movedIn: '01.05.2021'
    },
    {
      name: 'Frieder Albrecht',
      flat: 'Wohnung 4, 2. Stock',
      movedIn: '01.12.2020'
    },
    {
      name: 'Sandra Müller',
      flat: 'Wohnung 2, 1. Stock',
      movedIn: '01.07.2019'
    }
  ];

  flats = [
    {
      tentant: '-',
      flat: 'Wohnung 5, 2. Stock',
      size: '59 qm',
      rent: '900.- euro',
      caution: '1700.- euro'
    },
    {
      tentant: 'Max Musermann',
      flat: 'Wohnung 13, 5. Stock',
      size: '89 qm',
      rent: '1200.- euro',
      caution: '2000.- euro'
    },
    {
      tentant: 'Frieder Albrecht',
      flat: 'Wohnung 4, 2. Stock',
      size: '49 qm',
      rent: '890.- euro',
      caution: '1500.- euro'
    },
    {
      tentant: 'Sandra Müller',
      flat: 'Wohnung 2, 1. Stock',
      size: '62 qm',
      rent: '1000.- euro',
      caution: '1800.- euro'
    }
  ];

  functionaries = [
    {
      name: 'Johannes Blau',
      role: 'Hausverwaltung',
      phoneNumber: '+49 1543456'
    },
    {
      name: 'Eva Grün',
      role: 'Beirat Mitglied',
      phoneNumber: '+49 345937459'
    },
    {
      name: 'Hans Gelb',
      role: 'Hausmeister',
      phoneNumber: '+49 348579324'
    },
    {
      name: 'Peter Schwab',
      role: 'Eigentümer',
      phoneNumber: '+49 2345758'
    }
  ];

  meetings = [
    {
      type: 'Beiratssitzung',
      date: '14.09.2021 - 20:00',
      duration: '3 Stunden'
    },
    {
      type: 'ETV',
      date: '21.08.2021 - 20:00',
      duration: '3 Stunden'
    },
    {
      type: 'Vorbesprechung ETV',
      date: '14.08.2021 - 20:00',
      duration: '1,5 Stunden'
    }
  ];

  incomes = {
    labels: ['Miete/Jahr', 'Miete/Monat', 'Kaution'],
    datasets: [
      {
        label: 'Betrag in Euro',
        backgroundColor: '#42A5F5',
        borderColor: '#1E88E5',
        data: [576000, 48000, 80900]
      }
    ]
  };

  incidentGraphic: any;
  priorityGraphic: any;

  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private documentService: DocumentsService,
              private ticketService: TicketsService) {


  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.realEstate = this.documentService.loadRealEstate(id);
    this.data = {
      labels: ['Vermietet', 'Unvermietet'],
      datasets: [
        {
          data: [this.realEstate.numberOfRentFlats , this.realEstate.numberOfFlats - this.realEstate.numberOfRentFlats],
          backgroundColor: [
            '#009933',
            '#CC0000',
          ],
          hoverBackgroundColor: [
            '#009933',
            '#CC0000',
          ]
        }]
    };

    this.payments = {
      labels: ['Beglichene Zahlungen', 'Offene Zahlungen'],
      datasets: [
        {
          data: [20 , 17],
          backgroundColor: [
            '#009933',
            '#CC0000',
          ],
          hoverBackgroundColor: [
            '#009933',
            '#CC0000',
          ]
        }]
    };

    this.tickets = this.ticketService.loadTickets();

    this.incidentGraphic = {
      labels: ['Entwurf', 'In Arbeit', 'Abgsschlossen'],
      datasets: [
        {
          data: [
            this.tickets.filter(t => t.status.id === 0).length ,
            this.tickets.filter(t => t.status.id === 1).length,
            this.tickets.filter(t => t.status.id === 2).length],
          backgroundColor: [
            '#CC0000',
            '#FF9900',
            '#009933',
          ],
          hoverBackgroundColor: [
            '#CC0000',
            '#FF9900',
            '#009933',
          ]
        }]
    };

    this.priorityGraphic = {
      labels: ['Hoch (1)', 'Mittel (2)', 'Niedrig (3)'],
      datasets: [
        {
          data: [
            this.tickets.filter(t => t.priority.id === 1).length ,
            this.tickets.filter(t => t.priority.id === 2).length,
            this.tickets.filter(t => t.priority.id === 3).length],
          backgroundColor: [
            '#CC0000',
            '#FF9900',
            '#009933',
          ],
          hoverBackgroundColor: [
            '#CC0000',
            '#FF9900',
            '#009933',
          ]
        }]
    };

  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  showInGoogleMaps(): void {
    const url = 'https://www.google.de/maps/place/G%C3%A4rtnerpl.+1,+80469+M%C3%BCnchen/@48.1319409,11.5744517,17z/data=!3m1!4b1!4m5!3m4!1s0x479ddf6107bb6259:0xa7ed8cdac9e2a4fc!8m2!3d48.1319373!4d11.5766404';
    window.open(url);
  }

  goToIncidents(): void {
    this.routingService.navigateTickets();
  }
}
