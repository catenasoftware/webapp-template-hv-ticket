import { Injectable } from '@angular/core';
import {RoutingService} from '../routing/routing.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../users/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  constructor(private routingService: RoutingService, private httpClient: HttpClient) { }

  private username = 'catena-manager';
  private password = 'catena-manager';
  private isAuth = true;

  // private username: string;
  // private password: string;
  // private isAuth = false;

   getUsername(): string{
    return this.username;
  }

  setUserName(username: string): void{
     this.username = username;
  }

  getPassword(): string{
    return this.password;
  }

  isAuthenticated(): boolean{
    return this.isAuth;
  }

  tryAuthenticationOnServer(username: string): Promise<User>{
    return this.httpClient.get<User>('/api/users/auth/' + username).toPromise();
  }

  async authenticate(username: string, password: string): Promise<boolean>{
     this.setUserName(username);
     this.password = password;

     // if the authentication process will fail with username, user will be undefined
     const user = await this.tryAuthenticationOnServer(username).catch( _ => {
       //
     });

     if (user){
       this.isAuth = true;
     }
     return this.isAuthenticated();
  }

  signOut(): void{
     this.isAuth = false;
     this.setUserName('');
     this.password = '';
     this.routingService.navigateLogin();
  }

  signIn(): void{
    this.routingService.navigateHome();
  }
}
