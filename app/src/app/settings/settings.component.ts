import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../routing/routing.service';
import {AuthenticationService} from '../authentication/authentication.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  username: string;

  constructor(private routingService: RoutingService, private authenticationService: AuthenticationService) {
    this.username = this.authenticationService.getUsername();
  }

  ngOnInit(): void {
  }

  navigateHome(): void {
    this.routingService.navigateHome();
  }
}
