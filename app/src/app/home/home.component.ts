import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';
import {RoutingService} from '../routing/routing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  username: string;
  currentDate = new Date().toDateString();
  displayInfoDialog = false;

  constructor(private authenticationService: AuthenticationService, private routingService: RoutingService) {
    this.username = this.authenticationService.getUsername();
  }

  ngOnInit(): void {}

  signOut(): void {
    this.authenticationService.signOut();
  }

  navigateUsersTable(): void {
    this.routingService.navigateUsersTable();
  }

  showInfoDialog(): void {
    this.displayInfoDialog = true;
  }

  navigateSettings(): void {
    this.routingService.navigateSettings();
  }

  navigateFeedback(): void {
    this.routingService.navigateFeedback();
  }

  navigateDocuments(): void {
    this.routingService.navigateDocuments();
  }

  navigateCGround(): void {
    this.routingService.navigateRegisterTenat();
  }

  navigateTickets(): void {
    this.routingService.navigateTickets();
  }
}
