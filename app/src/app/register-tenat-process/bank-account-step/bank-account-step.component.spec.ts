import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankAccountStepComponent } from './bank-account-step.component';

describe('BankAccountStepComponent', () => {
  let component: BankAccountStepComponent;
  let fixture: ComponentFixture<BankAccountStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankAccountStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankAccountStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
