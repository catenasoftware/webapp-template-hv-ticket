import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {BankAccount, Tenant, TenatRegisterServiceService} from '../tenat-register-service.service';

@Component({
  selector: 'app-bank-account-step',
  templateUrl: './bank-account-step.component.html',
  styleUrls: ['./bank-account-step.component.scss']
})
export class BankAccountStepComponent implements OnInit {

  constructor(private routingService: RoutingService, private tenantRegisterService: TenatRegisterServiceService) { }

  tenants: Tenant[];
  bankAccount: BankAccount;

  ngOnInit(): void {
    this.tenants = this.tenantRegisterService.loadTenants();
    this.bankAccount = this.tenantRegisterService.loadBankAccount();
  }

  nextStep(): void {

  }

  prevPage(): void {
    this.routingService.navigateTenantStep();
  }

  nextPage(): void {
    this.tenantRegisterService.saveBankAccount(this.bankAccount);
  }
}
