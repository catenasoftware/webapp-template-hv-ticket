import { Injectable } from '@angular/core';


export interface Tenant {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: string;
  birthPlace: string;
  phoneNumber: string;
  mobile: string;
  mail: string;
  moveInDate: string;
  moveOutDate: string;
  username: string;
}

export interface BankAccount {
  id: number;
  accountOwner: Tenant;
  iban: string;
  bic: string;
}


@Injectable({
  providedIn: 'root'
})
export class TenatRegisterServiceService {

  tenantsStep: Tenant[] = [
      {
        id: 0,
        firstName: '',
        lastName: '',
        birthDate: '',
        birthPlace: '',
        phoneNumber: '',
        mobile: '',
        mail: '',
        moveInDate: '',
        moveOutDate: '',
        username: '',
      }
  ];

  bankAccountStep: BankAccount = {
    id: 0,
    accountOwner: undefined,
    iban: '',
    bic: '',
  };

  constructor() { }

  saveTenants(tenatns: Tenant[]): void {
    this.tenantsStep = tenatns;
  }

  loadTenants(): Tenant[] {
    return this.tenantsStep;
  }

  saveBankAccount(bankAccount: BankAccount): void {
    this.bankAccountStep = bankAccount;
  }

  loadBankAccount(): BankAccount {
    return this.bankAccountStep;
  }
}
