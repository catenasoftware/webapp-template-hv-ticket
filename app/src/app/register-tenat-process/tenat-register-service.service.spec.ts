import { TestBed } from '@angular/core/testing';

import { TenatRegisterServiceService } from './tenat-register-service.service';

describe('TenatRegisterServiceService', () => {
  let service: TenatRegisterServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TenatRegisterServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
