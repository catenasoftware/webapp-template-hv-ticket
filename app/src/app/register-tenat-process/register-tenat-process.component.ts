import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../routing/routing.service';

@Component({
  selector: 'app-register-tenat-process',
  templateUrl: './register-tenat-process.component.html',
  styleUrls: ['./register-tenat-process.component.scss']
})
export class RegisterTenatProcessComponent implements OnInit {
  items: any;

  constructor(private routingService: RoutingService) { }

  ngOnInit(): void {
    this.items = [
      // {label: 'Wohneinheit'},
      {
        label: 'Bewohner',
        routerLink: 'tenant-step'
      },
      {
        label: 'Bankdaten',
        routerLink: 'bank-step'
      },
      {label: 'Wohneinheit'},
      {label: 'Wohnungsgeberbestätigung'},
      {label: 'Prüfung'},
    ];
    this.routingService.navigateTenantStep();
  }

  navigateBack(): void {
    this.routingService.navigateHome();
  }
}
