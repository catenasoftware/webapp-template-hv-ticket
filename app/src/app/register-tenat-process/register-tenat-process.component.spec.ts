import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTenatProcessComponent } from './register-tenat-process.component';

describe('RegisterTenatProcessComponent', () => {
  let component: RegisterTenatProcessComponent;
  let fixture: ComponentFixture<RegisterTenatProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterTenatProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTenatProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
