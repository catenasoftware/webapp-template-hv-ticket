import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenatDataComponent } from './tenat-data.component';

describe('TenatDataComponent', () => {
  let component: TenatDataComponent;
  let fixture: ComponentFixture<TenatDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenatDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenatDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
