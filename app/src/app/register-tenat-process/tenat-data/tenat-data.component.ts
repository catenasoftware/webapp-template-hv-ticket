import { Component, OnInit } from '@angular/core';
import {Tenant, TenatRegisterServiceService} from '../tenat-register-service.service';
import {RoutingService} from "../../routing/routing.service";

@Component({
  selector: 'app-tenat-data',
  templateUrl: './tenat-data.component.html',
  styleUrls: ['./tenat-data.component.scss']
})
export class TenatDataComponent implements OnInit {

  personalInformation: any;

  submitted = false;
  creatTenatAccount = false;

  tenants: Tenant[];

  constructor(private tenantRegisterService: TenatRegisterServiceService, private routingService: RoutingService) { }

  ngOnInit(): void {
    this.tenants = this.tenantRegisterService.loadTenants();
    console.log(this.tenantRegisterService.loadTenants());
  }

  addTenant(): void {
    this.tenants.push(
      {
        id: this.tenants.length - 1,
        firstName: '',
        lastName: '',
        birthDate: '',
        birthPlace: '',
        phoneNumber: '',
        mobile: '',
        mail: '',
        moveInDate: '',
        moveOutDate: '',
        username: '',
      }
    );
  }

  removeTenant(id: number): void {
    this.tenants = this.tenants.filter( e => e.id !== id);
  }

  nextStep(): void {
    console.log(this.tenants);
    this.tenantRegisterService.saveTenants(this.tenants);
    this.routingService.navigateBankAccountStep();
  }
}
