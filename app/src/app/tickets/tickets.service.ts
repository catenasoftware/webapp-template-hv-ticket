import { Injectable } from '@angular/core';

export interface Ticket {
  id: string;
  ticketNumber: string;
  name: string;
  status: Status;
  category: Category[];
  dateCreated: string;
  creator: User;
  editor: User;
  livingUnit: LivingUnit;
  description: string;
  priority: Priority;
  comments: Comment[];
}

export interface Comment {
  id: number;
  text: string;
  author: User;
}

export interface LivingUnit {
  id: number;
  displayName: string;
}

export interface Action {
  status: Status;
  text: string;
}

export interface Status {
  id: number;
  displayName: string;
}

export interface Category {
  id: number;
  displayName: string;
}

export interface User {
  id: number;
  displayName: string;
  role: string;
}

export interface Priority {
  id: number;
  displayName: string;
  priority: number;
}

export interface TicketUpdate {
  status: Status;
  description: string;
}

export interface SearchParams {
  ticketNumber: string;
  name: string;
  status: number;
  priority: number;
  category: number;
  editor: number;
  creator: number;
  livingUnit: number;
}


@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  users = [
    {id: 0, displayName: 'Paul W.', role: 'Mieter'},
    {id: 1, displayName: 'Susanne O.', role: 'Eigentümer'},
    {id: 2, displayName: 'Chris', role: 'Hausverwaltung'}];

  tickets = [
    {
      id: 'a',
      ticketNumber: 'T-1',
      name: 'Licht im Gang defekt',
      status: {id: 0, displayName: 'Entwurf'},
      dateCreated: '18.04.2021 - 16:45',
      priority: {id: 2, displayName: 'Mittel (2)', priority: 2},
      category: [{id: 0, displayName: 'Elektrik'}, {id: 1, displayName: 'Beleuchtung'}],
      creator: this.users[0],
      editor: this.users[1],
      livingUnit: {id: 1, displayName: 'Gärtnerplatz'},
      description: 'Das Lickt im EG ist seit 2 Tagen defekt. Bitte schnell reparieren, nachts ist es besonders dunkel',
      comments: [
        {
          id: 0,
          text: 'Ich kümmer mich!',
          author: this.users[1]
        }
      ]
    },
    {
      id: 'b',
      ticketNumber: 'T-2',
      name: 'Wasserhahn tropft',
      priority: {id: 3, displayName: 'Niedrig (3)', priority: 3},
      status: {id: 1, displayName: 'In Arbeit'},
      category: [{id: 2, displayName: 'Wasser'}],
      dateCreated: '18.04.2021 - 16:45',
      creator: this.users[1],
      editor: this.users[0],
      livingUnit: {id: 0, displayName: 'Maxvorstadt'},
      description: 'Das Lickt im EG ist seit 2 Tagen defekt. Bitte schnell reparieren, nachts ist es besonders dunkel',
      comments: [
        {
          id: 0,
          text: 'Ich kümmer mich!',
          author: this.users[1],
        }
      ]
    },
    {
      id: 'c',
      ticketNumber: 'T-3',
      name: 'Wasserhahn tropft',
      priority: {id: 3, displayName: 'Niedrig (3)', priority: 3},
      status: {id: 1, displayName: 'In Arbeit'},
      category: [{id: 2, displayName: 'Wasser'}],
      dateCreated: '18.04.2021 - 16:45',
      creator: this.users[1],
      editor: this.users[0],
      livingUnit: {id: 0, displayName: 'Maxvorstadt'},
      description: 'Das Lickt im EG ist seit 2 Tagen defekt. Bitte schnell reparieren, nachts ist es besonders dunkel',
      comments: [
        {
          id: 0,
          text: 'Ich kümmer mich!',
          author: this.users[1],
        }
      ]
    },
    {
      id: 'd',
      ticketNumber: 'T-4',
      name: 'Wasserhahn tropft',
      priority: {id: 3, displayName: 'Niedrig (3)', priority: 3},
      status: {id: 2, displayName: 'Abgeschlossen'},
      category: [{id: 2, displayName: 'Wasser'}],
      dateCreated: '18.04.2021 - 16:45',
      creator: this.users[0],
      editor: this.users[1],
      livingUnit: {id: 0, displayName: 'Maxvorstadt'},
      description: 'Das Lickt im EG ist seit 2 Tagen defekt. Bitte schnell reparieren, nachts ist es besonders dunkel',
      comments: [
        {
          id: 0,
          text: 'Ich kümmer mich!',
          author: this.users[1],
        }
      ]
    },
  ];

  constructor() { }

  loadTickets(): Ticket[] {
    return this.tickets;
  }

  loadTicketsById(id: string): Ticket {
    return this.tickets.find(t => t.ticketNumber === id);
  }

  loadStatus(): Status[] {
    return [{id: 0, displayName: 'Entwurf'}, {id: 1, displayName: 'In Arbeit'}, {id: 2, displayName: 'Abgeschlossen'}];
  }

  loadPriority(): Priority[] {
    return [{id: 1, displayName: 'Hoch (1)', priority: 1}, {id: 2, displayName: 'Mittel (2)', priority: 2}, {id: 3, displayName: 'Niedrig (3)', priority: 3}];
  }

  loadUsers(): User[] {
    return [{id: 0, displayName: 'Paul W.', role: 'Mieter'}, {id: 1, displayName: 'Susanne O.', role: 'Eigentümer'}, {id: 2, displayName: 'Chris', role: 'Hausverwaltung'}];
  }

  loadLivingUnits(): LivingUnit[] {
    return [{id: 0, displayName: 'Maxvorstadt'}, {id: 1, displayName: 'Gärtnerplatz'}];
  }

  loadCategory(): Category[] {
    return [
      {id: 0, displayName: 'Elektrik'},
      {id: 1, displayName: 'Beleuchtung'},
      {id: 2, displayName: 'Wasser'},
      {id: 3, displayName: 'Heizung'},
      {id: 4, displayName: 'Außenbereich'}];
  }

  updateTicket(ticketUpdate: Ticket): void {
    this.tickets[this.tickets.findIndex(t => t.ticketNumber === ticketUpdate.ticketNumber)] = ticketUpdate;
  }

  updateTicketPriority(ticketId: string, priority: Priority): void {
    this.tickets.find(ticket => ticket.id === ticketId).priority = priority;
  }

  addComment(comment: Comment): void {
    this.tickets[0].comments.push(comment);
  }

  searchTickets(searchParams: SearchParams): Ticket[] {
    let ticketsReturn = this.tickets;

    if (searchParams.ticketNumber) {
      ticketsReturn = ticketsReturn.filter( t => t.ticketNumber.toLowerCase().includes(searchParams.ticketNumber.toLowerCase()));
    }

    if (searchParams.name) {
      ticketsReturn = ticketsReturn.filter( t => t.name.toLowerCase().includes(searchParams.name.toLowerCase()));
    }

    if (searchParams.status !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.status.id === searchParams.status);
    }

    if (searchParams.priority !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.priority.priority === searchParams.priority);
    }

    if (searchParams.category !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.category.find(category => category.id === searchParams.category));
    }

    if (searchParams.editor !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.editor.id === searchParams.editor);
    }

    if (searchParams.creator !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.creator.id === searchParams.creator);
    }

    if (searchParams.livingUnit !== -1) {
      ticketsReturn = ticketsReturn.filter( t => t.livingUnit.id === searchParams.livingUnit);
    }

    return ticketsReturn;
  }

}
