import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {TicketsService, Ticket, SearchParams, Status, Category, Priority, User, LivingUnit} from '../tickets.service';


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {
  cols = [
    {field: 'ticketNumber', header: 'Ticket Nummer'},
    // {field: 'name', header: 'Name'},
    {field: 'name', header: 'Name'},
    {field: 'dateCreated', header: 'Erstelldatum'},
    {field: 'status', header: 'Status'},
  ];
  selectedTicket: Ticket;
  tickets: Ticket[];
  displaySearchBox = true;

  searchParams: SearchParams = {
    ticketNumber: '',
    name: '',
    status: -1,
    priority: -1,
    category: -1,
    editor: -1,
    creator: -1,
    livingUnit: -1
  };
  availableStatus: Status[];
  selectedSearchStatus: Status;
  availablePriority: Priority[];
  selectedSearchPriority: Priority;
  availableCategory: Category[];
  selectedSearchCategory: Category;
  availableLivingUnits: LivingUnit[];
  selectedLivingUnit: LivingUnit;
  availableVisibility: any[] = [ {id: 0, displayName: 'Hausverwaltung'}, {id: 1, displayName: 'Beirat'}];
  selectedVisibility: any;
  availableUser: User[];
  selectedSearchCreator: User;
  selectedSearchEditor: User;


  constructor(private routingService: RoutingService, private ticketService: TicketsService) { }

  ngOnInit(): void {
    this.tickets = this.ticketService.loadTickets();
    this.availableStatus = this.ticketService.loadStatus();
    this.availablePriority = this.ticketService.loadPriority();
    this.availableCategory = this.ticketService.loadCategory();
    this.availableUser = this.ticketService.loadUsers();
    this.availableLivingUnits = this.ticketService.loadLivingUnits();
  }

  navigateBack(): void {
    this.routingService.navigateBack();
  }

  viewTicket(): void {
    this.routingService.navigateTicket(this.selectedTicket.ticketNumber);
  }

  searchTickets(): void {
    // Status
    if (this.selectedSearchStatus) {
      this.searchParams.status = this.selectedSearchStatus.id;
    } else {
      this.searchParams.status = -1;
    }
    // Priority
    if (this.selectedSearchPriority) {
      this.searchParams.priority = this.selectedSearchPriority.priority;
    } else {
      this.searchParams.priority = -1;
    }
    // Category
    if (this.selectedSearchCategory) {
      this.searchParams.category = this.selectedSearchCategory.id;
    } else {
      this.searchParams.category = -1;
    }
    // Editor
    if (this.selectedSearchEditor) {
      this.searchParams.editor = this.selectedSearchEditor.id;
    } else {
      this.searchParams.editor = -1;
    }
    // Creator
    if (this.selectedSearchCreator) {
      this.searchParams.creator = this.selectedSearchCreator.id;
    } else {
      this.searchParams.creator = -1;
    }
    // LivingUnit
    if (this.selectedLivingUnit){
      this.searchParams.livingUnit = this.selectedLivingUnit.id;
    } else {
      this.searchParams.livingUnit = -1;
    }

    this.tickets = this.ticketService.searchTickets(this.searchParams);
  }

  resetSearch(): void {
    this.searchParams.ticketNumber = '';
    this.searchParams.name = '';
    this.selectedSearchStatus = null;
    this.searchParams.status = -1;
    this.selectedSearchEditor = null;
    this.searchParams.editor = -1;
    this.selectedSearchCreator = null;
    this.searchParams.creator = -1;
    this.selectedSearchPriority = null;
    this.searchParams.priority = -1;
    this.selectedSearchCategory = null;
    this.searchParams.category = -1;
    this.searchParams.livingUnit = -1;
    this.selectedLivingUnit = null;
    this.tickets = this.ticketService.loadTickets();
  }

  triggerSearchBox(): void {
    this.displaySearchBox = !this.displaySearchBox;
  }

  categoryString(category: Category[]): string {
    return category.map(e => e.displayName).join(', ');
  }
}
