import { Component, OnInit } from '@angular/core';
import {RoutingService} from '../../routing/routing.service';
import {
  Category,
  Comment,
  LivingUnit,
  Priority,
  Status,
  Ticket,
  TicketsService,
  TicketUpdate,
  User
} from '../tickets.service';
import {ActivatedRoute} from '@angular/router';
import {AppNotificationService} from '../../notification/app-notification.service';

@Component({
  selector: 'app-ticket-view',
  templateUrl: './ticket-view.component.html',
  styleUrls: ['./ticket-view.component.scss']
})
export class TicketViewComponent implements OnInit {

  ticket: Ticket;
  displayCreateActionDialog = false;
  availableStatus: Status[];
  availableLivingUnits: LivingUnit[];
  selectedStatus: string;
  ticketId: string;

  availableUser: User[];
  availableCategory: Category[];
  availablePriority: Priority[];

  ticketUpdate: TicketUpdate = {
    status: undefined,
    description: '',
  };

  updateComment: Comment = {
    id: -1,
    text: '',
    author: undefined
  };

  editComment = false;



  editText = false;


  constructor(private routingService: RoutingService,
              private route: ActivatedRoute,
              private ticketService: TicketsService,
              private notificationService: AppNotificationService) { }

  ngOnInit(): void {
    this.ticketId = this.route.snapshot.paramMap.get('ticketNumber');
    this.ticket = this.ticketService.loadTicketsById(this.ticketId);
    this.ticketUpdate.description = this.ticket.description;
    this.availableStatus = this.ticketService.loadStatus();
    this.availableCategory = this.ticketService.loadCategory();
    this.availablePriority = this.ticketService.loadPriority();
    this.availableUser = this.ticketService.loadUsers();
    this.availableLivingUnits = this.ticketService.loadLivingUnits();
  }

  navigateHome(): void {
    this.routingService.navigateBack();
  }


  updateTicketDescription(): void {
      this.ticketService.updateTicket(this.ticket);
      this.notificationService.addSuccess('Ticket wurde erfolgreich gespeichert.');
      this.ticket = this.ticketService.loadTicketsById(this.ticketId);
      this.editText = false;
  }

  editTextField(): void {
    this.editText = true;
  }

  cancelUpdateTicket(): void {
    this.editText = false;
    this.ticketUpdate.description = this.ticket.description;
  }

  triggerEditComment(id: number, text: string): void {
    this.updateComment.id = id;
    this.updateComment.text = text;
    this.editComment = true;
  }

  cancelEditComment(): void {
    this.updateComment.id = undefined;
    this.updateComment.text = undefined;
    this.editComment = false;
  }

  triggerUpdateComment(): void {
    this.ticket.comments.find(c => c.id === this.updateComment.id).text = this.updateComment.text;
    this.ticketService.updateTicket(this.ticket);
    this.cancelEditComment();
  }

  updateTicket(): void {
    this.ticketService.updateTicket(this.ticket);
  }

  createComment(): void {
    this.ticket.comments.push({
      id: 123,
      text: '',
      author: {
        id: 123,
        displayName: 'Paul',
        role: '123',
      },
    });
  }
}
