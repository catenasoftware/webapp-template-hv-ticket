import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../authentication/authentication.service';
import {RoutingService} from '../routing/routing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  message: string;

  constructor(private authenticationService: AuthenticationService, private routingService: RoutingService) { }

  ngOnInit(): void {
  }

  async login(): Promise<void> {
    if (!this.username || !this.password){
      this.message = 'Please enter Username and Password.';
    }
    await this.authenticationService.authenticate(this.username, this.password);
    if (this.authenticationService.isAuthenticated()){
      this.authenticationService.signIn();
    }else{
      this.message = 'Wrong Username or Password.';
    }
  }

    navigateCGround(): void {
        this.routingService.navigateAccountBoard();
    }
}
